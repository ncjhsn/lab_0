import http from 'http';

console.log('\nAtividade [1]');
console.log(at1(1, 100));

console.log('\nAtividade [3]');
console.log(at3(5,100));

console.log('\nAtividade [5]');
console.log(at5('alguma coisa :^)'));

console.log('\nAtividade [6]');
console.log(at6([1,2,3,4,5,10]));







function at1(x: number, y: number): number[] {
    let interval: number[] = new Array;
    for (x; x < y + 1; x++) {
        if (x % 2 === 0) {
            interval.push(x);
        }
    }
    return interval;
}

function at3(x: number, y: number): number | string {
    if (x < y) {
        return x;
    } else if (y < x) {
        return y;
    } else {
        return 'valores iguais';
    }
}

function at5(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
}

function at6(arr: number[]): number {
    arr.sort((a, b) => a.valueOf() - b.valueOf());
    return arr[arr.length - 1];
}
